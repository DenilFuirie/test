import React, {useState} from 'react';
import db from './db.json';
import axios from "axios";
import './app.css'

function App() {
    const [title, setTitle] = useState('')
    const [editTitle, setEditTitle] = useState('')
    const [img, setImg] = useState('')
    const [editImg, setEditImg] = useState('')
    const [togglePopup, setTogglePopup] = useState(false)
    const [movieId, setMovieId] = useState(null)

    const [cardList, setCardList] = useState(db.posts)


    const showPopup = (obj) => {
        setTogglePopup(!togglePopup)
        setMovieId(obj.id)
    }
    console.log(movieId)
    const imageHandler = (e) => {
        const reader = new FileReader();
        reader.onload = () =>{
            if(reader.readyState === 2){
                setImg(reader.result)
            }
        }
        reader.readAsDataURL(e.target.files[0])
    };

    const editImageHandler = (e) => {
        const reader = new FileReader();
        reader.onload = () =>{
            if(reader.readyState === 2){
                setEditImg(reader.result)
            }
        }
        reader.readAsDataURL(e.target.files[0])
    }

    function inputTitleHandler(e) {
        setTitle(e.target.value)
    }

    const inputEditTitleHandler = (e) => {
        setEditTitle(e.target.value)
    }

    const addItem = (value, img) => {
        axios.post('http://localhost:3001/posts', {
            id: Date.now(),
            title: value,
            author: "typicode",
            img: img
        })
    }

    const deleteItem = (id) => {
        axios.delete(`http://localhost:3001/posts/${id}`)
    }

    const editItem = (title) => {
        axios.patch(`http://localhost:3001/posts/${movieId}`, {
            title: title,
            img: editImg
        })
    }
  return (
    <div>
        <div>
            <input type="text" value={title} placeholder="name" onChange={(e) => inputTitleHandler(e)} /> <br/>
            <input type="file" accept="image/*" name="image-upload" id="input" onChange={imageHandler} />
            <button onClick={() => addItem(title, img)} type="submit">submit</button>
        </div>
        <div className="cards">
            {togglePopup &&
                <div className="popup">
                <h1>Редактирование</h1>
                <div className="inputs">
                    <input type="text" value={editTitle} onChange={inputEditTitleHandler}/>
                    <input type="file" accept="image/*" name="image-upload" id="input" onChange={editImageHandler} />
                    <button onClick={() => editItem(editTitle)} type="submit">submit</button>
                </div>
            </div>
            }


            {db.posts.map((obj, id) => {
                return <div
                    className="card"
                    draggable={true}

                >
                    <button onClick={() => deleteItem(obj.id)}>X</button>
                    <button onClick={() => showPopup(obj)}>edit</button>
                    <img src={obj.img} width={200} height={400}/>
                    <p>{obj.title}</p>
                </div>
            })}
        </div>

    </div>
  );
}

export default App;
